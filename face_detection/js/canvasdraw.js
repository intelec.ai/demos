colors = ['#f44336', '#E91E63', '#9C27B0', '#673AB7', '#3F51B5', '#2196F3', '#03A9F4',
  '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEB3B', '#FFC107',
  '#FF9800', '#FF5722'];
const ratios = [20 / 13, 84 / 48];
const caps = ["pics/hat1.png", "pics/hat2.png"];
const hatIndex = 1;

function putHatOnHead(canvas, result) {
  const context = canvas.getContext('2d');
  context.lineWidth = Math.floor((canvas.width + canvas.height) / 300);
  const fontSize = Math.floor(0.03 * canvas.height + 0.5);
  context.font = fontSize + 'px Arial';

  result.forEach(prediction => {
    if (prediction['predicted_class'] == "face" && prediction['probability'] > 0.8) {
      drawRectangle(context, prediction, fontSize);
      const box = prediction['box'];
      var img = new Image();
      img.src = caps[hatIndex];

      var faceRectangleWidth = box[2] - box[0];
      var width = faceRectangleWidth * 1.2;
      var height = width / ratios[hatIndex];
      var startPointX = box[0] - faceRectangleWidth * 0.1;
      var startPointY = box[1] - height + height * 0.4;
      context.drawImage(img, startPointX, startPointY, width, height);
    }
  });
}

//future delete this method
function drawRectangle(context, prediction, fontSize) {
  const label = prediction['predicted_class'] + ' ' + Math.round(prediction['probability'] * 100) + '%';
  const labelSize = fontSize * label.length * 0.7;
  const color = this.colors[this.ramdomNumberBetween(0, 0)];

  // draw rectangles
  context.beginPath();

  context.strokeStyle = color;
  context.fillStyle = color;

  const box = prediction['box'];
  context.rect(box[0], box[1], box[2] - box[0], box[3] - box[1]);

  const fillRectLeft = box[0] - context.lineWidth / 2;
  const fillRectTop = (box[1] - fontSize > 0 ? box[1] - fontSize : box[1] + 1);
  const fillRectHeight = fontSize * 1.2;
  context.fillRect(fillRectLeft, fillRectTop, labelSize, fillRectHeight);

  context.closePath();
  context.stroke();

  // draw label
  context.fillStyle = 'black';
  context.fillText(label, box[0] + 1, fillRectTop + fillRectHeight * 0.8);
}

function ramdomNumberBetween(lo, hi) {
  return Math.floor((Math.random() * hi) + lo);
}