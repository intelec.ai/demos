function uploadImage(canvas, callBack) {
    var file = dataURLtoBlob(canvas.toDataURL());
    var formData = new FormData();
    formData.append('image', file);
    // sendImageData(canvas, formData);

    sendImageData(formData, callBack);
}

dataURLtoBlob = function (dataURL) {
    var binary = atob(dataURL.split(',')[1]);
    var array = [];
    var i = 0;
    while (i < binary.length) {
        array.push(binary.charCodeAt(i));
        i++;
    }
    return new Blob([new Uint8Array(array)], { type: 'image/png' });
}

function sendImageData(formData, callBack) {
    // return new Promise(function (resolve, reject) {
        
    $.ajax({
        type: 'POST',
        url: 'https://demo.intelec.ai:4441/api/v1/models/3/inference',
        data: formData,
        processData: false,
        contentType: false,
        // contentType: 'multipart/form-data',

        success: function (data, status, jQxhr) {
            // putHatOnHead(canvas, data.result);
            callBack(data.result);
        },
        error: function (jqXhr, status, errorThrown) {

        }
    });
    // });
}