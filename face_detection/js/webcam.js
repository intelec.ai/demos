navigator.getUserMedia = (navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

var canvas, context, video;

function init() {
    video = document.getElementById('video');
    canvas = document.getElementById("canvas");
    context = canvas.getContext('2d');
    startWebcam();
}

function startWebcam() {
    if (navigator.getUserMedia) {
        navigator.getUserMedia({
            video: true,
            audio: false
        }, function (localMediaStream) {
            //video.src = window.URL.createObjectURL(localMediaStream);
            video.srcObject = localMediaStream;

            this.video.addEventListener('play', function () {
                videoPlayCallback();
            }, false);

        }, function (err) {
            console.log("Error : " + err);
        });
    } else {
        console.log("Media is not supported");
    }
}
var isStarted = false;

function videoPlayCallback() {
    if (this.video.paused || this.video.ended) {
        return;
    }
    context.drawImage(this.video, 0, 0, this.video.width, this.video.height);
    if (!isStarted) {
        isStarted = true;
        uploadImageContinously();
    }

    drawHeadsContinously();

    setTimeout(function () {
        videoPlayCallback();
    }, 0);
}

var heads;
function uploadImageContinously() {
    uploadImage(canvas, (data) => {
        heads = data;
    });

    setTimeout(function () {
        uploadImageContinously();
    }, 3000);
}

function drawHeadsContinously() {
    if (heads != null && heads.length > 0) {
        putHatOnHead(canvas, heads);
    }
}